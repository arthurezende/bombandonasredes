<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Bombando nas Redes</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="body">
  <?php
    INCLUDE_ONCE("form.php");
  ?>
<!-- <div id="loader"></div> -->
<!-- <div  id="myDiv" class="animate-bottom"> -->
  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-none d-lg-block p-0">
    <div class="container clearfix">
      <div class="contact-info float-left pt-2">
        <h5 class="mb-0 align-top"> <i class="fa fa fa-phone p-0"></i> (34) 99216-7062</h5>
      </div>
      <div class="social-links float-right">
        <a href="https://www.instagram.com/turbinesuasredes/" class="instagram"><i class="fa fa-2x fa-instagram"></i></a>
        <a href="https://www.youtube.com/channel/UCdCsh8zIphceaRLRa2jumtA" class="youtube"><i class="fa fa-2x fa-youtube-play"></i></a>
      </div>
    </div>
  </section>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto"> <img src="img/logobombando.png" style="max-width: 90px" class="img-fluid" alt=""></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>
      <div id="logo" class="pull-left">
        <a href="http://painel.bombandonasredes.com.br" class="btn-get-started scrollto">Cadastre-se</a>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>
      <div id="logo" class="pull-left">
        <a href="http://painel.bombandonasredes.com.br" class="btn-projects scrollto">Login</a>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>
      <!-- <a href="http://painel.bombandonasredes.com.br" class="btn-get-started scrollto">Cadastre-se</a> -->
      <!-- <a href="http://painel.bombandonasredes.com.br" class="btn-projects scrollto">Login</a> -->
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#body"><i class="fa fa-home"></i> Home</a></li>
          <!-- <li><a href="#about"><i class="fa fa-instagram"></i> Instagram</a></li> -->
          <li class="menu-has-children"><a href=""><span class="fa fa-instagram"></span> </a>
            <ul>
              <li>Instagram</li>
              <li><a href="#">Alcance para postagens</a></li>
              <li><a href="#">Comentários aleatórios Brasileiros</a></li>
              <li><a href="#">Curtidas com impressões</a></li>
              <li><a href="#">Seguidores brasileiros reais</a></li>
              <li><a href="#">Story views</a></li>
              <li><a href="#">Visitas no perfil</a></li>
              <li><a href="#">Curtidas com Impressões</a></li>
              <li><a href="#">Live Curtidas</a></li>
              <li><a href="#">Live Visualizações</a></li>
              <li><a href="#">Seguidores Brasileiros por Sexo</a></li>
              <li><a href="#">Visualizações em Vídeo com Impressões</a></li>
              <li><a href="#">E MUITO MAIS!</a></li>
            </ul>
          </li>
          <li class="menu-has-children"><a href=""><span class="fa fa-facebook"></span> </a>
            <ul>
              <li>Facebook</li>
              <li><a href="#">Comentários brasileiros</a></li>
              <li><a href="#">Interessados em eventos</a></li>
              <li><a href="#">Likes para posts</a></li>
              <li><a href="#">Reações em fotos/posts</a></li>
              <li><a href="#">Visualizações em vídeo</a></li>
              <li><a href="#">Seguidores para perfil</a></li>
              <li><a href="#">Reação Amei em Posts/Fotos</a></li>
              <li><a href="#">Reação Grr/Bravo em Posts/Fotos</a></li>
              <li><a href="#">E MUITO MAIS!</a></li>
            </ul>
          </li>
          <li class="menu-has-children"><a href=""><span class="fa fa-youtube-play"></span> </a>
            <ul>
              <li>Youtube</li>
              <li><a href="#">Inscritos para canal</a></li>
              <li><a href="#">Visualizações em vídeo</a></li>
              <li><a href="#">Likes em vídeos</a></li>
              <li><a href="#">Likes em comentários</a></li>
              <li><a href="#">Visualizações Alta Qualidade</a></li>
              <li><a href="#">Visualizações REAIS Brasileiros</a></li>
              <li><a href="#">E MUITO MAIS!</a></li>
            </ul>
          </li>
          <li class="menu-has-children"><a href=""><span class="fa fa-twitter"></span> </a>
            <ul>
              <li>Twitter</li>
              <li><a href="#">Seguidores Mundiais Reais</a></li>
              <li><a href="#">Visualizações para Vídeo</a></li>
            </ul>
          </li>
          <li class="menu-has-children"><a href=""><span class="fa fa-globe"></span> </a>
            <ul>
              <li>Site</li>
              <li><a href="#">Visitas de Brasileiros</a></li>
              <li><a href="#">Visitas Mundiais</a></li>
            </ul>
          </li>
          <li class="menu-has-children"><a href=""><span class="fa fa-spotify"></span> </a>
            <ul>
              <li>Spotify</li>
              <li><a href="#">Plays para Música/Stream</a></li>
              <li><a href="#">Seguidores para Artista/bandas</a></li>
              <li><a href="#">Seguidores para Perfil</a></li>
              <li><a href="#">Seguidores para Playlist</a></li>
            </ul>
          </li>
          <li class="menu-has-children"><a href=""><span class="fa fa-soundcloud"></span> </a>
            <ul>
              <li>SoundCloud</li>
              <li><a href="#">Downloads de Música</a></li>
              <li><a href="#">Likes para Música/Playlist</a></li>
              <li><a href="#">Plays para Música/Stream</a></li>
              <li><a href="#">Reposts</a></li>
              <li><a href="#">Seguidores para Perfil</a></li>
            </ul>
          </li>
          <!-- <li><a href="#services"><i class="fa fa-facebook"></i> Facebook</a></li> -->
          <!-- <li><a href="#portfolio"><i class="fa fa-youtube"></i> Youtube</a></li> -->
          <li><a href="#"><i class="fa fa-question-circle-o"></i> FAQ</a></li>
          <li><a href="#contact">Contato</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">

   <div class="intro-content">
     <h2><span href="" class="typewrite" style="text-decoration:none;" data-period="2000" data-type='[ "TURBINE suas REDES", "Seu INSTAGRAM NO MODO TURBO ", "VALORIZE sua MARCA", "Transforme seu INSTAGRAM em um FUNCIONÁRIO" ]'>
         <span class="wrap"></span> </span></h2>
     <!-- <div>
       <a href="http://painel.bombandonasredes.com.br" class="btn-get-started scrollto">Cadastre-se</a>
       <a href="http://painel.bombandonasredes.com.br" class="btn-projects scrollto">Login</a>
     </div> -->
   </div>

   <div id="intro-carousel" class="owl-carousel" >
     <div class="item" style="background-image: url('img/intro-carousel/1.jpg');"></div>
     <div class="item" style="background-image: url('img/intro-carousel/2.jpg');"></div>
     <div class="item" style="background-image: url('img/intro-carousel/3.jpg');"></div>
     <div class="item" style="background-image: url('img/intro-carousel/4.jpg');"></div>
     <div class="item" style="background-image: url('img/intro-carousel/5.jpg');"></div>
   </div>

 </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Section
    ============================-->
    <section id="about" class="wow fadeInUp mb-0 pb-0 text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 about-img">
            <img src="img/handiphone.jpg" height="" alt="">
          </div>

          <div class="col-lg-6 content">
            <h2>Acesse vários serviços de impulsionamento para seu Instagram, Youtube, Facebook e mais!</h2>
            <h3>Com o instagram limitando a entrega das postagens em 7% para seus seguidores,
você irá alavancar o seu perfil e o seu negócio com o nosso Inovador Sistema.</h3>
<!-- <button class="btn btn-primary mb-3">Comece agora</button> -->
<a class="btn btn-primary" href="http://painel.bombandonasredes.com.br" role="button">Comece agora</a>
            <ul>
              <li><i class="ion-android-checkmark-circle"></i>
                Seguidores <b>reais</b></li>
              <li><i class="ion-android-checkmark-circle"></i>
                <b>Não precisamos</b> da sua Senha e  Login</li>
              <li><i class="ion-android-checkmark-circle"></i>
                <b>Aumente</b> as visitas no seu perfil</li>
              <li><i class="ion-android-checkmark-circle"></i>
                Seguidores <b>REAIS</b> brasileiros
              <li><i class="ion-android-checkmark-circle"></i>
                Opções para <b>IGTV</b> e <b>HISTÓRIAS</b>
              <li><i class="ion-android-checkmark-circle"></i>
                Likes e Visualizações em vídeo <b>AUTOMÁTICOS</b>
              <li><i class="ion-android-checkmark-circle"></i>
                Comentários <b>AUTOMÁTICOS</b>
            </ul>

          </div>
        </div>

      </div>
    </section>
    <section id="call-to-action" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Vamos lá!</h3>
            <p class="cta-text"> Basta fazer um cadastro, Adicionar Saldo e você já pode utilizar <b>todos</b> os Serviços de Impulsionamento.<br><br>Não precisamos de nenhuma informação sua, apenas o link do perfil ou da postagem. 100% Seguro e Confiável</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="http://painel.bombandonasredes.com.br">Começar</a>
          </div>
        </div>
      </div>
    </section>
    <section id="services" class="text-center" style="">
      <div class="container">
        <div class="section-header">
          <h2 class="">Por que nos escolher?</h2>
          <p>Entenda o motivo de sermos referência no mercado e o porque de nossos clientes nos escolherem.</p>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="box wow fadeInLeft">
              <div class="icon"><i class="fa fa-lock"></i></div>
              <h4 class="title"><a href="">Segurança</a></h4>
              <p class="description">Toda a informação dos nossos clientes é confidencial e privada.</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight">
              <div class="icon"><i class="fa fa-money"></i></div>
              <h4 class="title"><a href="">Preço</a></h4>
              <p class="description">Garantimos os melhores preços do mercado.</p><Br>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-users"></i></div>
              <h4 class="title"><a href="">Suporte</a></h4>
              <p class="description">Nós respondemos às suas perguntas com rapidez e eficiência.</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-check-circle-o"></i></div>
              <h4 class="title"><a href="">Confiança</a></h4>
              <p class="description">Já trabalhamos para um grande número de influenciadores e artistas. O que você está esperando?</p>
            </div>
          </div>

        </div>

      </div>
    </section>
    <section id="about" class="wow fadeInUp mb-0 pb-0 pt-3">
      <div class="container">
        <div class="row">

          <div class="col-lg-8 content mx-auto text-center">
            <h2 class="">Com nossas ferramentas você pode melhorar e atrair mais pessoas para o seu Perfil e NEGÓCIO.</h2>
          </div>
          <!-- segunda row -->

        </div>

      </div></section>
            <!-- #call-to-action -->
            <section id="about" class="wow fadeInUp mb-0 pb-5 pt-1">
              <div class="container">
                <div class="row">


                  <!-- segunda row -->

                </div>

              </div></section>
              <section id="about" class="wow fadeInUp mb-0 pb-5 pt-3">
                <div class="container">
                  <div class="row">

                    <div class="col-lg-8 content mx-auto text-center">
                      <h2>Nossos produtos são 100% seguros.</h2><h3> Não utilizamos bot ou nenhum outro programa para entregar o produto contratado.
          Em nenhum momento iremos solicitar a sua senha da rede social escolhida e além disso, os dados transmitidos através dos nossos servidores são cryptografados (https),
          o que garante para você maior segurança e privacidade.
          </h3>
                    </div>
                    <!-- segunda row -->

                  </div>

                </div>
                  <iframe src="https://www.youtube.com/embed/FoCNifJuw4Q?rel=0" class="w-100" height="500" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

              </section>


    <!--==========================
      Testimonials Section
    ============================-->
    <section id="testimonials" class="wow fadeInUp text-center">
      <div class="container">
        <div class="section-header">
          <h2>Depoimentos</h2>
          <p>Nosso objetivo é a satisfação de nossos clientes. Confira o que eles tem a dizer sobre sua experiência com nossos serviços:</p>
        </div>
        <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-item align-self-end">
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Deixo meu depoimento pra você que está analisando se pega ou não, este robô.
Excelente, 1000% de aumento no fluxo no meu Instagram em poucos meses.
Além da quantidade de seguidores, tive já várias parcerias fechadas, devido a essa ferramenta. Recomendo.
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
              <!-- <img src="img/testimonial-1.jpg" class="testimonial-img" alt=""> -->
              <h3>Rodrigo</h3>
              <h4>Médico</h4>
            </div>

            <div class="testimonial-item">
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Essa plataforma foi o combustível que meu maior cliente precisava para bombar e alavancar seu canal no Youtube!
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
              <!-- <img src="img/testimonial-2.jpg" class="testimonial-img" alt=""> -->
              <h3>Caroline</h3>
              <h4>Gestora de Redes Sociais</h4>
            </div>

            <div class="testimonial-item">
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Consegui aumentar minhas vendas através do Facebook utilizando a ferramenta para aumentar meus seguidores e engajamento nas publicações.
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
              <!-- <img src="img/testimonial-3.jpg" class="testimonial-img" alt=""> -->
              <h3>Carlos</h3>
              <h4>Corretor imobiliário</h4>
            </div>

        </div>

      </div>
    </section><!-- #testimonials -->

    <!-- <section id="clients" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Clientes</h2>
          <p>Conheça alguns de nossos clientes.</p>
        </div>

        <div class="owl-carousel clients-carousel">
          <img src="img/clients/client-1.png" alt="">
          <img src="img/clients/client-2.png" alt="">
          <img src="img/clients/client-3.png" alt="">
          <img src="img/clients/client-4.png" alt="">
          <img src="img/clients/client-5.png" alt="">
          <img src="img/clients/client-6.png" alt="">
          <img src="img/clients/client-7.png" alt="">
          <img src="img/clients/client-8.png" alt="">
        </div>

      </div>
    </section> -->

    <section id="call-to-action" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Experimente</h3>
            <p class="cta-text"> Comece agora e faça do seu Instagram <b>um funcionário</b>. Conheça nossos serviços e crie sua conta em nossa plataforma já!</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="http://painel.bombandonasredes.com.br">Começar</a>
          </div>
        </div>

      </div>
    </section><!-- #call-to-action -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="wow fadeInUp text-center">
      <div class="container">
        <div class="section-header">
          <h2>Entre em contato</h2>
          <p>Nossa equipe está pronta para tirar todas as suas dúvidas. Preencha o formulário abaixo e entre em contato conosco.</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="fa fa-envelope-o"></i>
              <h3>E-mail</h3>
              <p><a href="mailto:atendimento@bombandonasredes.com.br">atendimento@bombandonasredes.com.br</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="fa fa-whatsapp"></i>
              <h3>WhatsApp</h3>
              <p><a href="tel:+155895548855">(34) 99216-7062</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-address">
              <i class="fa fa-clock-o"></i>
              <h3>Horário de atendimento</h3>
              <p>Seg. à Sexta. das 9:00h às 19:00h.</p>
              <p>Sábado das 10:00h ás 14:00h.</p>
            </div>
          </div>
          <!-- <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:info@example.com">info@example.com</a></p>
            </div>
          </div> -->

        </div>
      </div>

      <div class="container">
        <!-- Uncomment below if you wan to use dynamic maps -->
        <!--<div id="google-map" data-latitude="40.713732" data-longitude="-74.0092704"></div>-->
      </div>

      <div class="container">
        <div class="form">

          <form action="contact.php" id="contact-form" method="post" class="contact-form">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="assunto" placeholder="Assunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensagem"></textarea>
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="submit" value="Enviar mensagem" class="btn btn-primary mb-3">
              <div class="messages"></div>
            </div>
          </form>
        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; <strong>Bombando nas Redes</strong>
      </div>
      <div class="container-fluid text-center">
        <img class="img-fluid" style="max-width:150px;" src="img/logopagseguro.png"/>
        <img class="img-fluid" style="max-width:150px;" src="img/paypal logo.png"/>
        <img class="img-fluid" style="max-width:150px;" src="img/siteblindado.png"/>
      </div>

    </div>
  </footer><!-- #footer -->
<!-- </div> -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="contact.js"></script>
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>
  <script src="validator.js"></script>
  <!-- Uncomment below if you want to use dynamic Google Maps -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script> -->

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
  <script src="js/typewriter.js"></script>
  <script>
  var myVar= setTimeout(showPage, 10);

  function showPage() {
    // document.getElementById("loader").style.display = "none";
    document.getElementById("topbar").style.display = "block";
    document.getElementById("header").style.display = "block";
    // $('#myDiv').css('display', 'none');
  }
  </script>
    <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "+5534992167062", // WhatsApp number
                call_to_action: "Entre em contato pelo WhatsApp", // Call to action
                position: "left", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
</body>
</html>
